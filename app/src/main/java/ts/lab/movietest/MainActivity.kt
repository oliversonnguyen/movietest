package ts.lab.movietest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import timber.log.Timber
import ts.lab.movietest.data.model.MovieList
import ts.lab.movietest.data.repo.MovieRepo
import ts.lab.movietest.ui.theme.MovieTestTheme
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var appString: String

    @Inject
    lateinit var repo: MovieRepo

    @Inject
    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MovieTestTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {

                    LaunchedEffect(lifecycle.coroutineScope) {
                        lifecycleScope.launch {
                            repeatOnLifecycle(Lifecycle.State.STARTED) {
                                viewModel.uiState.collect() {
                                    Timber.d(">>>ui state update: ${it.totalPages}")
                                }

                            }
                        }
                    }


                    val uiState: MovieList by viewModel.uiState.collectAsStateWithLifecycle()
                    val fav : Boolean by viewModel.favUiState.collectAsStateWithLifecycle()


                    Greeting("Android " + appString + fav, uiState = uiState) {
                        viewModel.updateSample()
                    }
                }
            }
        }
    }
}


@Composable
fun Greeting(name: String, modifier: Modifier = Modifier, uiState: MovieList = movieListDefault, onclick: () -> Unit) {
    Column {
        Text(
            text = "Hello $name!" + uiState.totalPages,
            modifier = modifier
        )
        Button(onClick = { onclick() }) {
            Text(text = "CLick me")
        }
    }

}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MovieTestTheme {
        Greeting("Android") {

        }
    }
}