package ts.lab.movietest

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import ts.lab.movietest.data.Result
import ts.lab.movietest.data.model.MovieList
import ts.lab.movietest.data.repo.MovieRepo

val movieListDefault: MovieList =
    MovieList(page = 0, emptyList(), totalPages = 0, totalResults = 0)

class MainViewModel(
    private val movieRepo: MovieRepo
) : ViewModel() {


    private val movieListState = MutableStateFlow(movieListDefault)
    val uiState: StateFlow<MovieList> = movieListState.stateIn(
        viewModelScope,
        SharingStarted.Eagerly,
        movieListState.value
    )

    private val favMutableState = MutableStateFlow(false)

    val favUiState: StateFlow<Boolean> = favMutableState

    init {
        viewModelScope.launch {
            val result = movieRepo.getTrendingMovies()
            when (result) {
                is Result.Success -> {
                    val data = result.data
                    movieListState.update { data }
                    Timber.d(">>>success: $data")
                }

                is Result.Error -> {
                    Timber.e(">>> error: ${result.exception}")
                }
            }

            movieRepo.observeFav(false)
                .collect {
                    favMutableState.update { it }
                }

        }


    }

    fun updateSample() {
        viewModelScope.launch {
            movieListState.update {
                it.copy(totalPages = it.totalPages + 1)
            }
            favMutableState.update {
                !it
            }
        }

    }

}