package ts.lab.movietest.data.remote

import retrofit2.Response
import retrofit2.http.GET
import ts.lab.movietest.data.model.MovieList

interface MovieService {
    @GET("3/trending/movie/week")
    suspend fun getTrendingMovies(): Response<MovieList>

}