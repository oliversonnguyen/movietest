package ts.lab.movietest.data.repo

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import ts.lab.movietest.data.Result
import ts.lab.movietest.data.model.Movie
import ts.lab.movietest.data.model.MovieList
import ts.lab.movietest.data.remote.MovieService

interface MovieRepo {
    suspend fun getTrendingMovies(): Result<MovieList>
    suspend fun observeFav(input: Boolean): Flow<Boolean>
}

class MovieRepoImpl(private val movieService: MovieService) : MovieRepo {
    override suspend fun getTrendingMovies(): Result<MovieList> {
        return withContext(Dispatchers.IO) {
            try {
                val response = movieService.getTrendingMovies()
                if (response.isSuccessful) {
                    val body: MovieList? = response.body()
                    if (body != null) {
                        Result.Success(body)
                    } else {
                        Result.Error(IllegalArgumentException(" ${response.code()} ${response.message()}"))
                    }
                } else {
                    Result.Error(IllegalArgumentException(" ${response.code()} ${response.message()}"))
                }

            } catch (exception: Exception) {
                Result.Error(exception)

            }
        }
    }

    override suspend fun observeFav(input: Boolean): Flow<Boolean> {
        return withContext(Dispatchers.IO) {
            flow {
                emit(input)
            }
        }
    }
}