package ts.lab.movietest.di

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Converter
import retrofit2.Retrofit
import ts.lab.movietest.data.remote.MovieService
import ts.lab.movietest.data.repo.MovieRepo
import ts.lab.movietest.data.repo.MovieRepoImpl
import ts.lab.movietest.network.RetrofitUtil
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideAText(): String = "Hello World 3"

    @Singleton
    @Provides
    fun provideGson(): Gson = RetrofitUtil.provideGson()

    @Singleton
    @Provides
    fun provideFactory(gson: Gson): Converter.Factory = RetrofitUtil.provideFactoryConverter(gson)

    @Singleton
    @Provides
    fun provideRetrofit(factoryConverter: Converter.Factory): Retrofit =
        RetrofitUtil.createRetrofit(factoryConverter)

    @Singleton
    @Provides
    fun provideMovieService(retrofit: Retrofit): MovieService =
        retrofit.create(MovieService::class.java)

    @Singleton
    @Provides
    fun provideMovieRepo(movieService: MovieService): MovieRepo {
        return MovieRepoImpl(movieService)
    }
}