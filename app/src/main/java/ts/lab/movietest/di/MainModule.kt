package ts.lab.movietest.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import ts.lab.movietest.MainViewModel
import ts.lab.movietest.data.repo.MovieRepo

@Module
@InstallIn(ActivityComponent::class)
object MainModule {
    @ActivityScoped
    @Provides
    fun provideMainViewModel(repo: MovieRepo): MainViewModel = MainViewModel(repo)
}