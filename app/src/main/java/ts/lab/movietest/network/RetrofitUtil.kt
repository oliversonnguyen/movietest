package ts.lab.movietest.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


const val BASE_URL = "https://api.themoviedb.org"

object RetrofitUtil {
    val token =
        "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0MDRlMTM0NGRmZDEzODUyYzg5YmM5OWI3NzQ3OWJmMCIsInN1YiI6IjU4MDM5Yzc3OTI1MTQxMWJhYTAwMjU2NSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.g0ZsMr2aChmIeRu_A-NNrGXQ_EqOfZTYXvqJk4U8OeA"
    val apiKey = "404e1344dfd13852c89bc99b77479bf0"
    fun provideGson(): Gson = GsonBuilder().create()
    fun provideFactoryConverter(gson: Gson): Converter.Factory =
        GsonConverterFactory.create(gson)

    var client = OkHttpClient.Builder().addInterceptor { chain ->
        val newRequest = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer $token")
            .build()
        chain.proceed(newRequest)
    }.build()

    fun createRetrofit(factory: Converter.Factory): Retrofit {

        return Retrofit.Builder()
            .client(client)
            .baseUrl(BASE_URL)
            .addConverterFactory(factory)
            .build()
    }

}